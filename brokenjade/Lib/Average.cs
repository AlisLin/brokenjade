﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thunder.Framework.Formula
{
    /// <summary>
    /// 平均值
    /// </summary>
    public class Average
    {
        protected struct ValueStruct
        {
            public int Flag;
            public DateTime Time;
            public double Value;
        }

        protected uint _Count;
        protected List<ValueStruct> Values;
        protected List<double> AveValues;
        protected double _AverageValue;
        protected double _AverageValueofSecond;
        protected int _CurPos;
        protected double _AverageValueofSecondSmooth;

        public Average()
        {
            _Count = 1;
            LoadValue(_Count);
        }

        public Average(uint count)
        {
            _Count = count;
            LoadValue(_Count);
        }

        /// <summary>
        /// 设置计算平均值长度
        /// </summary>
        public uint Count
        {
            get { return _Count; }
            set
            {
                _Count = value;
                LoadValue(_Count);
            }
        }

        /// <summary>
        /// 平均值
        /// </summary>
        public double AverageValue
        {
            get { return _AverageValue; }
        }

        /// <summary>
        /// 每秒平均值
        /// </summary>
        public double AverageValueofSecond
        {
            get { return _AverageValueofSecond; }
        }

        /// <summary>
        /// 每秒平滑值
        /// </summary>
        public double AverageValueofSecondSmooth
        {
            get { return _AverageValueofSecondSmooth; }
        }

        protected void LoadValue(uint count)
        {
            _CurPos = 0;
            if (Values == null)
            {
                Values = new List<ValueStruct>();
                AveValues = new List<double>();
            }
            else
            {
                Values.Clear();
                AveValues.Clear();
            }
            for (int i = 0; i < _Count; i++)
            {
                ValueStruct v = new ValueStruct();
                Values.Add(v);
                AveValues.Add(0);
            }

        }

        /// <summary>
        /// 计算平均值
        /// </summary>
        /// <param name="value">输入值</param>
        /// <returns></returns>
        public virtual double GetValue(double value)
        {
            return GetValue(value, DateTime.Now);
        }

        /// <summary>
        /// 计算平均值
        /// </summary>
        /// <param name="value">输入值</param>
        /// <param name="time">时间</param>
        /// <returns>平均值</returns>
        public virtual double GetValue(double value, DateTime time)
        {
            ValueStruct v = new ValueStruct();
            v.Flag = 1;
            v.Time = time;
            v.Value = value;
            Values[_CurPos] = v;
            //计算平均值
            Ave();
            _CurPos++;
            _CurPos = _CurPos < _Count ? _CurPos : 0;
            return _AverageValue;
        }

        protected void Ave()
        {
            double t1 = 0;
            double t2 = 0;
            DateTime mintime = DateTime.MaxValue;
            DateTime maxtime = DateTime.MinValue;
            double k = 0;
            for (int i = 0; i < _Count; i++)
            {
                if (Values[i].Flag==1)
                {
                    k++;
                    t1 += Values[i].Value;
                    t2 += AveValues[i];

                    if (mintime > Values[i].Time)
                    {
                        mintime = Values[i].Time;
                    }
                    if (maxtime < Values[i].Time)
                    {
                        maxtime = Values[i].Time;
                    }
                }
            }
            if (k>0)
            {
                //计算及时平均值
                _AverageValue = t1 / k;

                //计算每秒平均值
                double m = (maxtime - mintime).TotalSeconds;
                _AverageValueofSecond = t1 / m;

                //计算每秒平均值平滑值
                t2 -= AveValues[_CurPos];
                t2 += _AverageValueofSecond;
                AveValues[_CurPos] = _AverageValueofSecond;
                _AverageValueofSecondSmooth = t2 / k;
            }
        }
    }
}
