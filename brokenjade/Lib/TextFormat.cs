﻿using System;
using System.Text;

namespace Thunder.Framework.Text
{
    /// <summary>
    /// 文本处理
    /// </summary>
    public static class TextFormat
    {
        public const double KBYTE = 1024;
        public const double MBYTE = 1024 * 1024;
        public const double GBYTE = 1024 * 1024 * 1024;
        //public const double TBYTE = 1024 * 1024 * 1024 * 1024;

        /// <summary>
        /// 格式化输出字串（不截断字串）
        /// </summary>
        /// <param name="mainstr">输入字串</param>
        /// <param name="fillchar">填充字符</param>
        /// <param name="linelen">输出宽度</param>
        /// <returns></returns>
        public static string TextTab(string mainstr, char fillchar, int linelen)
        {
            return TextTab(mainstr, fillchar, linelen, false);
        }

        /// <summary>
        /// 格式化输出字串
        /// </summary>
        /// <param name="mainstr">输入字串</param>
        /// <param name="fillchar">填充字符</param>
        /// <param name="linelen">输出宽度</param>
        /// <param name="cutstring">是否截断字串(默认值:false)</param>
        /// <param name="filltype">填充模式(默认值:0，0:右侧填充，1:左侧填充)</param>
        /// <returns></returns>
        public static string TextTab(string mainstr, char fillchar, int linelen, bool cutstring=false, int filltype=0)
        {
            StringBuilder sb = new StringBuilder(mainstr, linelen);
            if (mainstr!=null)
            {
                if (mainstr.Length<linelen)
                {
                    switch (filltype)
                    {
                        default:
                        case 0:
                            //右侧填充
                            for (int i = mainstr.Length; i < linelen; i++)
                            {
                                sb.Append(fillchar);
                            }
                            break;
                        case 1:
                            //左侧填充
                            sb = new StringBuilder();
                            for (int i = 0; i < linelen - mainstr.Length; i++)
                            {
                                sb.Append(fillchar);
                            }
                            sb.Append(mainstr);
                            break;
                    }
                }
            }
            string m = "";
            if (cutstring)
            {
                m = sb.ToString(0, linelen);
            }
            else
            {
                m = sb.ToString();
            }
            return m;
        }

        /// <summary>
        /// 格式化输出字串（默认右侧填充）
        /// </summary>
        /// <param name="mainstr">输入字串</param>
        /// <param name="fillchar">填充字符</param>
        /// <param name="linelen">输出宽度</param>
        /// <param name="cutstring">是否截断字串</param>
        /// <returns></returns>
        public static string TextTab(string mainstr, char fillchar, int linelen, bool cutstring)
        {
            return TextTab(mainstr, fillchar, linelen, false,0);
        }

        /// <summary>
        /// 字串自动切换方法（队列限制：不能含有相同的元素）
        /// </summary>
        /// <param name="mstr">当前字串</param>
        /// <param name="SwitchArray">字串切换队列（队列限制：不能含有相同的元素）</param>
        /// <returns>下一个字串</returns>
        public static string TextSwitch(string mstr, string[] SwitchArray)
        {
            string result = mstr;
            bool flag = true;
            if (SwitchArray != null)
            {
                for (int i = 0; i < SwitchArray.Length; i++)
                {
                    if (SwitchArray[i] == mstr)
                    {
                        i++;
                        if (i >= SwitchArray.Length)
                        {
                            i = 0;
                        }
                        result = SwitchArray[i];
                        flag = false;
                        break;
                    }
                }

                if (flag)
                {
                    result = SwitchArray[0];
                }
            }

            return result;
        }

        /// <summary>
        /// 字串自动切换方法（队列无限制）
        /// </summary>
        /// <param name="Step">当前计数</param>
        /// <param name="SwitchArray">字串切换队列（队列无限制）</param>
        /// <returns>下一个字串</returns>
        public static string TextSwitch(ref int Step, string[] SwitchArray)
        {
            string result = "";
            if (SwitchArray != null)
            {
                Step++;
                if (Step >= SwitchArray.Length)
                {
                    Step = 0;
                }
                result = SwitchArray[Step];
            }

            return result;
        }

        /// <summary>
        /// 字串卷动方法
        /// </summary>
        /// <param name="mstr">原始字串</param>
        /// <param name="Step">运行计数</param>
        /// <param name="width">显示宽度</param>
        /// <param name="ScrollMode">卷动模式（0:左<-右，1:左->右，2:左<->右）</param>
        /// <returns></returns>
        public static string TextScroll(string mstr,ref int Step, int width, int ScrollMode=0)
        {
            string result = mstr;

            if (mstr!=null && width>0)
            {
                char[] sb = mstr.ToCharArray();
                char[] rb = new char[(width)*2+mstr.Length-2];

                for (int j = 0; j < rb.Length; j++)
                {
                    rb[j] = ' ';
                }
                Array.Copy(sb, 0, rb, width-1, sb.Length);
                int TotalStep = mstr.Length + width - 2;
                string[] SwitchString = null;
                
                switch (ScrollMode)
                {
                    default:
                    case 0:
                        //左<-右
                        TotalStep = mstr.Length + width - 1;
                        SwitchString = new string[TotalStep];
                        for (int i = 0; i < TotalStep; i++)
                        {
                            SwitchString[i] = new string(rb,i,width);
                        }
                      break;
                    case 1:
                        //左->右
                        TotalStep = mstr.Length + width - 1;
                        SwitchString = new string[TotalStep];
                        for (int i = 0; i < TotalStep; i++)
                        {
                            SwitchString[i] = new string(rb, TotalStep-i-1, width);
                        }
                        break;
                    case 2:
                        //左<->右
                        TotalStep = (mstr.Length + width - 1)*2;
                        SwitchString = new string[TotalStep];
                        int x = mstr.Length + width - 1;
                        for (int i = 0; i < x; i++)
                        {
                            SwitchString[i] = new string(rb,i,width);
                        }
                         for (int i = 0; i < x; i++)
                        {
                            SwitchString[i + x] = new string(rb, x - i - 1, width);
                        }
                        break;
                }

                result = TextSwitch(ref Step, SwitchString);
            }

            return result;
        }

        public static string ConvertByteString(ulong bytes)
        {
            string m = bytes.ToString("N0")+" Byte";
            double level = 800;
            double v1 = (double)bytes;
            double v2=0;
            v2 = v1;
            if (v2 > level)
            {
                v2 = v1 / KBYTE;
                if (v2 > level)
                {
                    v2 = v1 / MBYTE;
                    if (v2 > level)
                    {
                        v2 = v1 / GBYTE;
                        //if (v2 > level)
                        //{
                        //    v2 = v1 / TBYTE;
                        //    //按T处理
                        //    m = v2.ToString("N2") + " T";
                        //}
                        //else
                        //{
                        //    //按G处理
                        //    m = v2.ToString("N2") + " G";
                        //}
                        //按G处理
                        m = v2.ToString("N2") + " GB";
                    }
                    else
                    {
                        //按M处理
                        m = v2.ToString("N2") + " MB";
                    }
                }
                else
                {
                    //按K处理
                    m = v2.ToString("N2") + " KB";
                }
                
            }
            else
            {
                //按字节处理
            }
            return m;
        }

    }
}
