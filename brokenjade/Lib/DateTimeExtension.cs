namespace WebLib.Library
{
    using System;
    using System.Diagnostics;

    public static class DateTimeExtension
    {

        /// <summary>
        /// 输出持续时长：格式（n天n小时n分n秒）
        /// </summary>
        /// <param name="ts"></param>
        /// <param name="timeMode">输出模式（0:完整 / 1:统计到分钟 / 2:统计到小时 / 3:统计到天）</param>
        /// <param name="round">最后一位取整加权（0:无,0.5:四舍五入）</param>
        /// <returns></returns>
        public static string ToDurString(this TimeSpan ts, int timeMode = 0,double round=0)
        {
            var result = "";
            var n = ts.TotalDays;
            if (timeMode >= 3)
            {
                n += round;
                result += (int)n > 0 ? $"{(int)n}天" : "";
                return result;
            }
            else
            {
                result += (int)n > 0 ? $"{(int)n}天" : "";
            }

            n = ts.TotalHours;
            if (timeMode >= 2)
            {
                n += round;
                result += (int)n > 0 ? $"{((int)n % 24).ToString("00")}小时" : "";
                return result;
            }
            else
            {
                result += (int)n > 0 ? $"{((int)n % 24).ToString("00")}小时" : "";
            }

            n = ts.TotalMinutes;
            if (timeMode >= 1)
            {
                n += round;
                result += (int)n > 0 ? $"{((int)n % 60).ToString("00")}分" : "";
                return result;
            }
            else
            {
                result += (int)n > 0 ? $"{((int)n % 60).ToString("00")}分" : "";
            }
            n = ts.TotalSeconds;
            n += round;
            result += (int)n > 0 ? $"{((int)n % 60).ToString("00")}秒" : "";
            return result;
        }

    }
}