﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thunder.Framework.Formula
{
    /// <summary>
    /// 输入值为累计值模式平均值计算
    /// </summary>
    public class AverageTotalMode:Average
    {
        /// <summary>
        /// 上次记录值
        /// </summary>
        protected double _LastValue = 0;

        public AverageTotalMode()
        {
            _LastValue = 0;
            _Count = 5;
            LoadValue(_Count);
        }

        public AverageTotalMode(uint count)
        {
            _LastValue = 0;
            _Count = count;
            LoadValue(_Count);
        }

        /// <summary>
        /// 计算平均值
        /// </summary>
        /// <param name="value">累计值</param>
        /// <param name="time"></param>
        /// <returns></returns>
        public override double GetValue(double value, DateTime time)
        {
            double v = value - _LastValue;
            _LastValue = value;
            return base.GetValue(v, time);
        }
    }
}
