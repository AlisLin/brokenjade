﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace brokenjade
{
    class Program
    {
        static void Main(string[] args)
        {
            string d = "c";
            int len = 1024;
            if (args != null)
            {
                d = args[0];
                try
                {
                    len = Convert.ToInt32(args[1]);
                    len = len < 4096 ? 4096 : len;
                }
                catch (Exception ex)
                {
                }
            }
            Log($"目标驱动[{d}],长度[{len}]");

            JadeData jade = new JadeData();
            jade.Log += Jade_Log;
            jade.Create(d, len);

            Log("完成操作。");
        }

        private static void Jade_Log(string s)
        {
            Log(s);
        }

        static void Log(string s)
        {
            Console.WriteLine($"{DateTime.Now.ToString()}: {s}");
        }
    }
}
