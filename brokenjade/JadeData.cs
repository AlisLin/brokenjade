﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.IO;
using Thunder.Framework.Formula;
using WebLib.Library;

namespace brokenjade
{
    class JadeData
    {
        public delegate void LogHandle(string s);
        public event LogHandle Log;
        private long Current = 0;

        /// <summary>
        /// 当前处理进度（最大值100）
        /// </summary>
        public double ProcessPercent => Max == 0 ? 100 : Current * 100.0 / Max;
        public long Max { get; set; } = 0;

        private AverageTotalMode avg = new AverageTotalMode();

        public JadeData()
        {

        }

        public void Create(string drive = "",int size=4096)
        {
            //创建目录
            var root = Guid.NewGuid().ToString();
            drive = DefaultDrive(drive);
            root = Path.Combine(drive + ":\\", root);
            Directory.CreateDirectory(root);
            try
            {
                var remain = Remain(drive);
                Max = remain;
                
                while (remain > 1000000)
                {
                    List<Task> task = new List<Task>();
                    for (int i = 0; i < 1000; i++)
                    {
                        task.Add(Save(Path.Combine(root, Guid.NewGuid().ToString()), Generate(size)));
                    }
                    foreach (var item in task)
                    {
                        item.Wait();
                    }
                    remain = Remain(drive);
                    Current = Max - remain;
                    avg.GetValue(Current);
                    Log?.Invoke($"Remain = {remain.ToString("N0")}  Finish:{ProcessPercent.ToString("N2")}%   BPS:{BPS.ToString("N0")}  剩余时间：{RemainTime}");
                }
            }
            catch (Exception ex)
            {

                Log?.Invoke(ex.Message);
            }
            finally
            {
                //清理文件
                FileDelete(root);
            }
        }

        /// <summary>
        /// 文件清理
        /// </summary>
        /// <param name="root"></param>
        private void FileDelete(string root)
        {
            try
            {
                if (!Directory.Exists(root))
                {
                    return;
                }
                Log?.Invoke("开始清理……");
                var files = Directory.GetFiles(root);
                foreach (var item in files)
                {
                    File.Delete(item);
                }
                Directory.Delete(root);

                Log?.Invoke("清理完成。");
            }
            catch (Exception ex)
            {

                Log?.Invoke(ex.Message);
            }
        }

        public async Task<int> Save(string file,byte[] data)
        {
            Task task = Task.Run(()=> { File.WriteAllBytes(file, data); });
            await task;
            return 1;
        }

        public byte[] Generate(int len = 4096)
        {
            byte[] dat = new byte[len];
            Random r = new Random(DateTime.Now.Millisecond);
            r.NextBytes(dat);
            return dat;
        }

        /// <summary>
        /// 获取剩余空间
        /// </summary>
        /// <param name="drive"></param>
        /// <returns></returns>
        public long Remain(string drive = "")
        {
            drive = DefaultDrive(drive);
            ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\""+drive+":\"");
            disk.Get();
            var size = disk["FreeSpace"];
            return Convert.ToInt64(size);
        }

        /// <summary>
        /// 默认驱动器
        /// </summary>
        /// <param name="drive"></param>
        /// <returns></returns>
        private string DefaultDrive(string drive = "")
        {
            drive = string.IsNullOrWhiteSpace(drive) ? "c" : drive;
            return drive;
        }

        public double BPS => avg.AverageValueofSecondSmooth;

        public string RemainTime => new TimeSpan(0, 0, (int)((Max - Current) / avg.AverageValueofSecond)).ToDurString();
    }
}
